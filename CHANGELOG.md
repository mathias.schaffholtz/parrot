# [1.6.0](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.5.0...v1.6.0) (2024-04-19)


### Bug Fixes

* add fix titi ([abff119](https://gitlab.com/mathias.schaffholtz/parrot/commit/abff1194f3e54d4f7d828645dbf9ee3c11e9cbe8))
* add fix toto ([97b1707](https://gitlab.com/mathias.schaffholtz/parrot/commit/97b17075e0705210e7f274776c8e32f2f4130045))


### Features

* add feat 5 ([5f5b5dc](https://gitlab.com/mathias.schaffholtz/parrot/commit/5f5b5dc8a72a489d064bf58dcab708715fb5fa0b))

# [1.6.0-release.1](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.5.0...v1.6.0-release.1) (2024-04-19)


### Bug Fixes

* add fix titi ([abff119](https://gitlab.com/mathias.schaffholtz/parrot/commit/abff1194f3e54d4f7d828645dbf9ee3c11e9cbe8))
* add fix toto ([97b1707](https://gitlab.com/mathias.schaffholtz/parrot/commit/97b17075e0705210e7f274776c8e32f2f4130045))


### Features

* add feat 5 ([5f5b5dc](https://gitlab.com/mathias.schaffholtz/parrot/commit/5f5b5dc8a72a489d064bf58dcab708715fb5fa0b))

# [1.6.0-develop.2](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.6.0-develop.1...v1.6.0-develop.2) (2024-04-19)


### Bug Fixes

* add fix titi ([abff119](https://gitlab.com/mathias.schaffholtz/parrot/commit/abff1194f3e54d4f7d828645dbf9ee3c11e9cbe8))
* add fix toto ([97b1707](https://gitlab.com/mathias.schaffholtz/parrot/commit/97b17075e0705210e7f274776c8e32f2f4130045))

# [1.6.0-develop.1](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.5.0...v1.6.0-develop.1) (2024-04-17)


### Features

* add feat 5 ([5f5b5dc](https://gitlab.com/mathias.schaffholtz/parrot/commit/5f5b5dc8a72a489d064bf58dcab708715fb5fa0b))

# [1.5.0](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.4.1...v1.5.0) (2024-04-17)


### Features

* add feat 4 ([5a78dd4](https://gitlab.com/mathias.schaffholtz/parrot/commit/5a78dd42162abb7b4935dde338589d867096f02b))

# [1.5.0-release.1](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.4.1-release.1...v1.5.0-release.1) (2024-04-17)


### Features

* add feat 4 ([5a78dd4](https://gitlab.com/mathias.schaffholtz/parrot/commit/5a78dd42162abb7b4935dde338589d867096f02b))

# [1.5.0-develop.1](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.4.0...v1.5.0-develop.1) (2024-04-17)


### Bug Fixes

* add fix 3 ([165d933](https://gitlab.com/mathias.schaffholtz/parrot/commit/165d933ac8105ffcb3512dd215c23a6ec5c03c71))


### Features

* add feat 4 ([5a78dd4](https://gitlab.com/mathias.schaffholtz/parrot/commit/5a78dd42162abb7b4935dde338589d867096f02b))

# [1.4.0-develop.6](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.4.0-develop.5...v1.4.0-develop.6) (2024-04-17)


### Features

* add feat 4 ([5a78dd4](https://gitlab.com/mathias.schaffholtz/parrot/commit/5a78dd42162abb7b4935dde338589d867096f02b))

# [1.4.0-develop.5](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.4.0-develop.4...v1.4.0-develop.5) (2024-04-17)


### Bug Fixes

* add fix 3 ([165d933](https://gitlab.com/mathias.schaffholtz/parrot/commit/165d933ac8105ffcb3512dd215c23a6ec5c03c71))

# [1.4.0-develop.4](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.4.0-develop.3...v1.4.0-develop.4) (2024-04-17)


### Bug Fixes

* add fix 2 ([e45fe97](https://gitlab.com/mathias.schaffholtz/parrot/commit/e45fe97e1d111a31b261b5330ddafcacf0b669f5))


### Features

* add feat 2 ([38ab216](https://gitlab.com/mathias.schaffholtz/parrot/commit/38ab216df3354bbf69cc4233a1df88336bfd5715))

# [1.4.0-develop.3](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.4.0-develop.2...v1.4.0-develop.3) (2024-04-17)


### Features

* add release branch ([67cf8c1](https://gitlab.com/mathias.schaffholtz/parrot/commit/67cf8c19058eca86683cdd3f60e249acbaed4d6c))

# [1.4.0-develop.2](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.4.0-develop.1...v1.4.0-develop.2) (2024-04-17)


### Bug Fixes

* add fix 1 ([8439abd](https://gitlab.com/mathias.schaffholtz/parrot/commit/8439abd4028fd5b91bf490a5c98d2081f61b6799))


### Features

* add feat 1 ([6146f0e](https://gitlab.com/mathias.schaffholtz/parrot/commit/6146f0e84da0adf1ba8397a723ec11c890a46537))

# [1.4.0-develop.1](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.3.1...v1.4.0-develop.1) (2024-04-17)


### Bug Fixes

* modify maven plugin ([2279aa9](https://gitlab.com/mathias.schaffholtz/parrot/commit/2279aa99e104f4679e77035d349803c046ea4329))
* modify maven plugin ([5ff8a81](https://gitlab.com/mathias.schaffholtz/parrot/commit/5ff8a816201fb6e1dcd62f944c215389b3c9e5eb))


### Features

* prerelease branch ([5278c11](https://gitlab.com/mathias.schaffholtz/parrot/commit/5278c117a2dde9f86926c1cdb9b026b0e151a743))

## [1.3.1](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.3.0...v1.3.1) (2024-04-17)


### Bug Fixes

* add maven plugin ([7895099](https://gitlab.com/mathias.schaffholtz/parrot/commit/7895099d5f189ede6e79f2bc9965f7884ea0246e))

# [1.3.0](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.2.1...v1.3.0) (2024-04-17)


### Features

* add maven plugin ([b5565b7](https://gitlab.com/mathias.schaffholtz/parrot/commit/b5565b76220e9cbfb219a9936fbd16c55015217e))

## [1.2.1](https://gitlab.com/mathias.schaffholtz/parrot/compare/v1.2.0...v1.2.1) (2024-04-17)


### Bug Fixes

* add git plugin ([5d14c63](https://gitlab.com/mathias.schaffholtz/parrot/commit/5d14c630e667202b978adbca0976b23e79919509))
