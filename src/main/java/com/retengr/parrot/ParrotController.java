package com.retengr.parrot;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParrotController {

	@GetMapping("/repeat")
	public String repeat(@RequestParam(value = "value", defaultValue = "Bonjour Coco") String val) {
		return "Coco dit : "+val+"\n";
	}
}
